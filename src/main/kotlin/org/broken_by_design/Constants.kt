package org.broken_by_design

// x, y coordinates, i.e. horizontal and vertical
typealias Coordinate = Pair<Int, Int>

fun Coordinate.plus(two: Coordinate): Coordinate {
    return Coordinate(this.first + two.first, this.second + two.second )
}