package org.broken_by_design.neighbor

import org.broken_by_design.Coordinate
import org.broken_by_design.grid.Grid

fun interface Neighbor {

    /**
     * Returns list of all neighbours for the given coordinate.
     * *Depends on the selected algorithm.*
     *
     * @param grid to search for neighbors
     * @param coordinate to search for neighbors
     */
    fun getNeighbors(grid: Grid, coordinate: Coordinate): List<Coordinate>
}