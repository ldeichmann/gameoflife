package org.broken_by_design.neighbor

import org.broken_by_design.Coordinate
import org.broken_by_design.grid.Grid
import org.broken_by_design.plus
import java.util.stream.Collectors

/**
 * Implementation of neighbor algorithms using currying.
 */
class Neighbors {

    companion object {
        private val mooreOffsets = listOf(
            Coordinate(1, 0),
            Coordinate(0, 1),
            Coordinate(-1, 0),
            Coordinate(0, -1)
        )

        private val neumannOffsets = mooreOffsets + listOf(
            Coordinate(1, 1),
            Coordinate(-1, -1),
            Coordinate(1, -1),
            Coordinate(-1, 1)
        )

        val baseNeighbors = { offsets: List<Coordinate> ->
            { grid: Grid, coordinate: Coordinate ->
                offsets.stream().map {
                    coordinate.plus(it)
                }.filter { grid.isInside(it) }.collect(Collectors.toList())
            }
        }

        val moore = Neighbor(baseNeighbors(mooreOffsets))
        val neumann = Neighbor(baseNeighbors(neumannOffsets))


    }
}