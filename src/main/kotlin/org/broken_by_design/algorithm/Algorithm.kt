package org.broken_by_design.algorithm

import org.broken_by_design.grid.Grid
import org.broken_by_design.neighbor.Neighbor

fun interface Algorithm {
    /**
     * Runs one iteration of the algorithm on the given grid, using the provided neighbor mechanism.
     *
     * @param grid to perform transformation in place
     * @param neighbors to determine neighbors
     */
    fun runIteration(grid: Grid, neighbors: Neighbor)

    /**
     * Perform multiple iterations at once.
     *
     * @param grid to perform transformation in place
     * @param neighbors to determine neighbors
     * @param iterations number of iterations to perform.
     */
    fun runIterations(grid: Grid, neighbors: Neighbor, iterations: Int) {
        for (x in 0 until iterations) {
            runIteration(grid, neighbors)
        }
    }
}