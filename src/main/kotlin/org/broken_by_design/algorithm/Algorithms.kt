package org.broken_by_design.algorithm

import org.broken_by_design.Coordinate
import org.broken_by_design.grid.Grid
import org.broken_by_design.neighbor.Neighbor
import org.broken_by_design.neighbor.Neighbors


/**
 * Algorithm implementations using currying.
 */
class Algorithms {

    fun interface Curry {
        fun apply(alive: Boolean, numAliveNeighbors: Int): Boolean?
    }

    companion object {
        private val baseAlgorithm = { algo: Curry ->
            { grid: Grid, neighbors: Neighbor ->
                val updates = mutableMapOf<Coordinate, Boolean>()
                // collect all changes
                grid.seq().forEach { (coordinate, alive) ->
                    // collect neighbor aliveness
                    val numAliveNeighbors = neighbors.getNeighbors(grid, coordinate)
                        .map { grid.isAlive(it) }.count { it == true }

                    val result = algo.apply(alive, numAliveNeighbors)

                    // update on change and presence
                    result?.let {
                        if (it != alive) {
                            // update on change
                            updates[coordinate] = result
                        }
                    }
                }

                // apply changes
                updates.forEach { (coordinate, alive) ->
                    grid.setAlive(coordinate, alive)
                }
            }
        }

        val conway = Algorithm { grid, neighbors ->

            val extendedAlgo = { alive: Boolean, numAliveNeighbors: Int ->
                // apply the rules
                // alive and 2 or 3 neighbors
                if (alive && (numAliveNeighbors !in setOf(2, 3))) {
                    false
                // dead and three neighbors
                } else if (!alive && numAliveNeighbors == 3) {
                    true
                // don't care, no change
                } else {
                    null
                }
            }

            baseAlgorithm(extendedAlgo)(grid, neighbors)
        }

        /**
         * Parity will always use neumann neighbors.
         */
        val parity = Algorithm { grid, _ ->
            val extendedAlgo = { _: Boolean, numAliveNeighbors: Int ->
                numAliveNeighbors % 2 == 1
            }

            baseAlgorithm(extendedAlgo)(grid, Neighbors.neumann)
        }
    }
}