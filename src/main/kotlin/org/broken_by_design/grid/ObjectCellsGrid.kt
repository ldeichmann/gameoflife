package org.broken_by_design.grid

import org.broken_by_design.Coordinate

class ObjectCellsGrid(width: Int, height: Int) : Grid {

    private var data: List<List<Cell>>

    class Cell {
        var isAlive = false;
    }

    init {
        if (width <= 0 || height <= 0) {
            error("Bad input")
        }

        // create height many cells
        val columnSeq = generateSequence {
            // cell sequence
            val cellSeq = generateSequence { Cell() }

            cellSeq.take(height).toList()
        };
        this.data = columnSeq.take(width).toList();
    }

    override fun getDimensions(): Coordinate {
        return Pair(data.size, data[0].size)
    }

    override fun isAlive(coordinate: Coordinate): Boolean? {
        val col = data.getOrNull(coordinate.first)
        return col?.let {
            it.getOrNull(coordinate.second)?.isAlive
        }
    }

    override fun setAlive(coordinate: Coordinate, alive: Boolean): Boolean {
        val col = data.getOrNull(coordinate.first)
        val cell = col?.getOrNull(coordinate.second);

        return cell?.let { it.isAlive = alive; true } ?: false
    }
}