package org.broken_by_design.grid

import org.broken_by_design.Coordinate
import java.util.stream.Collectors
import kotlin.streams.toList

class GridUtil {

    companion object {

        private val toBoolMap = mapOf(
            "0" to false,
            "1" to true
        )

        private val toStringMap = toBoolMap.entries.associateBy({ it.value }) { it.key }

        /**
         * Load a grid config, where every line represents a row. Sizes must match
         */
        fun loadGridConfiguration(grid: Grid, config: String) {

            // determine config parameters
            val lines = config.lines()
            val columns = lines.stream().map { it.length }.collect(Collectors.toSet());
            // all columns must be the same size
            check(columns.size == 1)
            val columnSize = columns.stream().findFirst().get()

            // dimensions must match
            val (gridWidth, gridHeight) = grid.getDimensions();
            check(gridWidth == columnSize)
            check(gridHeight == lines.size)

            // only zero and one allowed
            check(lines.stream().map { it.chunked(1).stream() }.allMatch { it.allMatch { char -> toBoolMap.keys.contains(char) } } )
            val boolLines: List<List<Boolean>> = lines.stream().map {
                it.chunked(1).map { chr -> toBoolMap[chr] ?: error("Broken") }.toList();
            }.toList()

            boolLines.forEachIndexed { row, line ->
                    line.forEachIndexed { column, aliveValue -> grid.setAlive(Pair(column, row), aliveValue) }
            }
        }

        fun gridToString(grid: Grid) : String {
            var output = ""
            val (width, height) = grid.getDimensions()

            for (h in 0 until height) {
                for (w in 0 until width) {
                    val value = toStringMap[grid.isAlive(Coordinate(w, h))]
                    output += value
                }
                output += "\n"
            }

            return output.trimEnd('\n')
        }
    }

}