package org.broken_by_design.grid

import org.broken_by_design.Coordinate

class ArrayGrid(val width: Int, val height: Int) : Grid {

    private var data: Array<Boolean>

    init {
        if (width <= 0 || height <= 0) {
            throw Exception("Bad input")
        }

        this.data = Array(width * height) { false }
    }


    override fun getDimensions(): Coordinate {
        return Pair(width, height)
    }

    override fun isAlive(coordinate: Coordinate): Boolean? {
        if (!isInside(coordinate)) {
            return null
        }

        return data[coordinate.second * width + coordinate.first];
    }

    override fun setAlive(coordinate: Coordinate, alive: Boolean): Boolean {
        if (!isInside(coordinate)) {
            return false
        }

        data[coordinate.second * width + coordinate.first] = alive
        return true
    }
}