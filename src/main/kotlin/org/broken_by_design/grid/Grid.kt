package org.broken_by_design.grid

import org.broken_by_design.Coordinate

// I mean, honestly, this is overly complicated already and I hate it.
/**
 * A grid. All cells should initialize with 0.
 */
interface Grid {

    /**
     * Return the dimensions of the grid.
     */
    fun getDimensions(): Coordinate;


    /**
     * Determine whether cell at coordinates is alive. Null if no cell exists at coordinates.
     * @param coordinate to check for, width and height
     * @return true if alive, false if dead, null if outside of grid
     */
    fun isAlive(coordinate: Coordinate): Boolean?

    /**
     * Sets the alive status of a cell.
     * @param coordinate cell to set alive status for, width and height
     * @param alive alive status
     * @return true if succesful, false if no such cell exists
     */
    fun setAlive(coordinate: Coordinate, alive: Boolean): Boolean

    /**
     * Determines whether the given coordinate is inside the grid bounds.
     */
    fun isInside(coordinate: Coordinate): Boolean {
        val (width, height) = this.getDimensions()
        return coordinate.first in 0 until width
                && coordinate.second in 0 until height
    }

    /**
     * Generates a sequence to iterate over all entries row by row.
     */
    fun seq(): Sequence<Pair<Coordinate, Boolean>> {
        val (width, height) = getDimensions()
        return sequence<Pair<Coordinate, Boolean>> {
            for (x in 0 until width) {
                for (y in 0 until height) {
                    val currentCoordinate = Coordinate(x, y)
                    yield(Pair(currentCoordinate, isAlive(currentCoordinate)!!))
                }
            }
        }
    }
}