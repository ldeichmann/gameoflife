package org.broken_by_design.algorithm

import org.broken_by_design.grid.ArrayGrid
import org.broken_by_design.grid.GridUtil
import org.broken_by_design.grid.ObjectCellsGrid
import org.broken_by_design.neighbor.Neighbors
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertEquals

class ParityTest {

    private val parity = Algorithms.parity
    private val moore = Neighbors.moore
    private val neumann = Neighbors.neumann

    @Test
    fun smallStuckGrid() {
        val grid = ObjectCellsGrid(2, 2)
        val start = listOf(
            "10",
            "01"
        ).joinToString(separator = "\n")

        GridUtil.loadGridConfiguration(grid, start)
        assertEquals(start, GridUtil.gridToString(grid))

        for (x in 0 until 10) {
            parity.runIteration(grid, neumann)
            assertEquals(start, GridUtil.gridToString(grid))
        }
    }

    @Test
    fun smallGrid() {
        val grid = ObjectCellsGrid(2, 2)
        val start = listOf(
            "10",
            "00"
        ).joinToString(separator = "\n")
        val next = listOf(
            "01",
            "11"
        ).joinToString(separator = "\n")

        GridUtil.loadGridConfiguration(grid, start)
        assertEquals(start, GridUtil.gridToString(grid))

        for (x in 0 until 10) {
            parity.runIteration(grid, neumann)
            assertEquals(next, GridUtil.gridToString(grid))

            parity.runIteration(grid, neumann)
            assertEquals(start, GridUtil.gridToString(grid))
        }
    }


    @Test
    fun mediumGrid() {
        val grid = ObjectCellsGrid(5, 2)
        val start = listOf(
            "10101",
            "01010"
        ).joinToString(separator = "\n")
        val next = listOf(
            "11011",
            "00100"
        ).joinToString(separator = "\n")

        GridUtil.loadGridConfiguration(grid, start)
        assertEquals(start, GridUtil.gridToString(grid))

        for (x in 0 until 10) {
            parity.runIteration(grid, neumann)
            assertEquals(next, GridUtil.gridToString(grid))

            parity.runIteration(grid, neumann)
            assertEquals(start, GridUtil.gridToString(grid))
        }
    }
}