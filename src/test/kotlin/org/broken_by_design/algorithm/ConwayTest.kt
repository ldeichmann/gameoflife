package org.broken_by_design.algorithm

import org.broken_by_design.grid.ArrayGrid
import org.broken_by_design.grid.GridUtil
import org.broken_by_design.neighbor.Neighbor
import org.broken_by_design.neighbor.Neighbors
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class ConwayTest {

    private var neumann: Neighbor = Neighbors.neumann
    private var conway: Algorithm = Algorithms.conway

    @Test
    fun testBlinkerNeumann() {
        val grid = ArrayGrid(5, 5)
        val start = listOf(
            "00000", "00100", "00100", "00100", "00000"
        ).joinToString(separator = "\n")
        val next = listOf(
            "00000", "00000", "01110", "00000", "00000"
        ).joinToString(separator = "\n")

        GridUtil.loadGridConfiguration(grid, start)
        assertEquals(start, GridUtil.gridToString(grid))

        for (x in 0 until 10) {
            conway.runIteration(grid, neumann)
            assertEquals(next, GridUtil.gridToString(grid))

            conway.runIteration(grid, neumann)
            assertEquals(start, GridUtil.gridToString(grid))
        }

        conway.runIterations(grid, neumann, 2)
        assertEquals(start, GridUtil.gridToString(grid))

        conway.runIterations(grid, neumann, 3)
        assertEquals(next, GridUtil.gridToString(grid))
    }

    @Test
    fun testToadNeumann() {
        val grid = ArrayGrid(6, 6)
        val start = listOf(
            "000000", "000000", "001110", "011100", "000000", "000000"
        ).joinToString(separator = "\n")
        val next = listOf(
            "000000", "000100", "010010", "010010", "001000", "000000"
        ).joinToString(separator = "\n")

        GridUtil.loadGridConfiguration(grid, start)
        assertEquals(start, GridUtil.gridToString(grid))

        for (x in 0 until 10) {
            conway.runIteration(grid, neumann)
            assertEquals(next, GridUtil.gridToString(grid))

            conway.runIteration(grid, neumann)
            assertEquals(start, GridUtil.gridToString(grid))
        }

        conway.runIterations(grid, neumann, 2)
        assertEquals(start, GridUtil.gridToString(grid))

        conway.runIterations(grid, neumann, 3)
        assertEquals(next, GridUtil.gridToString(grid))
    }

}