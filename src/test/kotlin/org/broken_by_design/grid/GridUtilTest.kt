package org.broken_by_design.grid

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertEquals

class GridUtilTest {

    private val width = 3
    private val height = 4
    private val grid = ArrayGrid(width, height)


    @Test
    fun loadGridCorrect() {
        val content = grid.seq().toList()
        assertEquals(width * height, content.size)
        assertEquals(setOf(false), content.map { it.second }.toSet())

        val gridToLoad = "101\n001\n111\n000"

        GridUtil.loadGridConfiguration(grid, gridToLoad)
        assertEquals(gridToLoad, GridUtil.gridToString(grid))
    }

    @Test
    fun loadGridTooSmall() {
        run {
            val gridToLoad = "1011\n0011\n1111\n0001"
            assertThrows<Exception> { GridUtil.loadGridConfiguration(grid, gridToLoad) }
        }
        run {
            val gridToLoad = "101\n001\n111"
            assertThrows<Exception> { GridUtil.loadGridConfiguration(grid, gridToLoad) }
        }
    }

    @Test
    fun loadGridIrregularSize() {
        run {
            val gridToLoad = "101\n00\n111\n000"
            assertThrows<Exception> { GridUtil.loadGridConfiguration(grid, gridToLoad) }
        }
        run {
            val gridToLoad = ""
            assertThrows<Exception> { GridUtil.loadGridConfiguration(grid, gridToLoad) }
        }
    }

    @Test
    fun loadGridInvalidCharacters() {
        val gridToLoad = "101\n000\n\uD83C\uDF0811\n000"
        assertThrows<Exception> { GridUtil.loadGridConfiguration(grid, gridToLoad) }
    }

    @Test
    fun loadGridRightDirection() {
        val grid = ObjectCellsGrid(5, 5)
        val start = listOf(
            "00000", "00100", "00100", "00100", "00000"
        ).joinToString(separator = "\n")

        GridUtil.loadGridConfiguration(grid, start)
        assertEquals(start, GridUtil.gridToString(grid))
    }

}