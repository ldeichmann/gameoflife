package org.broken_by_design.grid

import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import kotlin.Exception
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

/**
 * Test all grid implementations.
 */
class GridTest {

    companion object {
        private const val height: Int = 500
        private const val width: Int = 400;

        @JvmStatic
        fun grids() = listOf(
            ObjectCellsGrid(width, height),
            ArrayGrid(width, height)
        )
    }

    @Test
    fun testInvalidDimensions() {
        assertThrows(Exception::class.java) { ObjectCellsGrid(-1, 10) }
        assertThrows(Exception::class.java) { ObjectCellsGrid(10, -1) }
        assertThrows(Exception::class.java) { ObjectCellsGrid(-1, -1) }
    }

    @ParameterizedTest
    @MethodSource("grids")
    fun testDefaultNotAlive(grid: Grid) {
        for (currentWidth in 0 until width) {
            for (currentHeight in 0 until height) {
                val alive = grid.isAlive(Pair(currentWidth, currentHeight));
                assertNotNull(alive, "Width $currentWidth height $currentHeight")
                assertFalse(alive, "Width $currentWidth height $currentHeight")
            }
        }
    }

    @ParameterizedTest
    @MethodSource("grids")
    fun testSetAlive(grid: Grid) {
        val x = 50;
        val y = 40;

        var isAlive = grid.isAlive(Pair(x, y))
        assertNotNull(isAlive)
        assertFalse { isAlive!! }

        assertTrue(grid.setAlive(Pair(x, y), true));

        isAlive = grid.isAlive(Pair(x, y))
        assertNotNull(isAlive)
        assertTrue { isAlive!! }

        assertTrue(grid.setAlive(Pair(x, y), false));

        isAlive = grid.isAlive(Pair(x, y))
        assertNotNull(isAlive)
        assertFalse { isAlive }
    }

    @ParameterizedTest
    @MethodSource("grids")
    fun testSetAliveOutOfRange(grid: Grid) {
        assertFalse {grid.setAlive(Pair(width + 1, height), true) };
        assertFalse {grid.setAlive(Pair(width, height + 1), true) };
        assertFalse {grid.setAlive(Pair(width + 1, height + 1), true) };
    }

    @ParameterizedTest
    @MethodSource("grids")
    fun testIsAliveOutOfRange(grid: Grid) {
        assertNull(grid.isAlive(Pair(width + 1, height)));
        assertNull(grid.isAlive(Pair(width, height + 1)));
        assertNull(grid.isAlive(Pair(width + 1, height + 1)));
    }

}