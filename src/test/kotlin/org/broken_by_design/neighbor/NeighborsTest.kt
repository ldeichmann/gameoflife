package org.broken_by_design.neighbor

import org.broken_by_design.Coordinate
import org.broken_by_design.grid.ArrayGrid
import org.broken_by_design.grid.Grid
import org.broken_by_design.grid.GridUtil
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class NeighborsTest {

    private var neumann: Neighbor =  Neighbors.neumann
    private var moore: Neighbor = Neighbors.moore
    private var grid: Grid
    private val field: List<String> = listOf(
        "000000",
        "101010",
        "010101",
        "000000"
    )

    private val gridWidth = field[0].length
    private val gridHeight = field.size


    init {
        grid = ArrayGrid(gridWidth, gridHeight)
        GridUtil.loadGridConfiguration(grid, field.joinToString(separator = "\n"))
    }

    @Test
    fun testOriginNeighbors() {
        // moore coordinates
        val mooreOriginCords: Set<Coordinate> = setOf(Coordinate(1, 0), Coordinate(0, 1))
        // neumann coordinates
        val neumannOriginCoords: Set<Coordinate> = setOf(Coordinate(1, 0), Coordinate(1, 1), Coordinate(0, 1))

        val mooreNeighbors: List<Coordinate> = moore.getNeighbors(grid, Coordinate(0, 0))
        assertEquals(mooreOriginCords, mooreNeighbors.toSet())

        val neumannNeighbors: List<Coordinate> = neumann.getNeighbors(grid, Coordinate(0, 0))
        assertEquals(neumannOriginCoords, neumannNeighbors.toSet())
    }

    @Test
    fun testMaxLimitNeighbors() {
        val bottomRight = Coordinate(gridWidth - 1, gridHeight - 1)

        // moore coordinates
        val mooreOriginCords: Set<Coordinate> = setOf(
            Coordinate(bottomRight.first - 1, bottomRight.second),
            Coordinate(bottomRight.first, bottomRight.second - 1),
        )
        // neumann coordinates
        val neumannOriginCoords: Set<Coordinate> = setOf(
            Coordinate(bottomRight.first - 1, bottomRight.second),
            Coordinate(bottomRight.first, bottomRight.second - 1),
            Coordinate(bottomRight.first - 1, bottomRight.second - 1),
        )

        val mooreNeighbors: List<Coordinate> = moore.getNeighbors(grid, bottomRight)
        assertEquals(mooreOriginCords, mooreNeighbors.toSet())

        val neumannNeighbors: List<Coordinate> = neumann.getNeighbors(grid, bottomRight)
        assertEquals(neumannOriginCoords, neumannNeighbors.toSet())
    }

    @Test
    fun testCenterNeighbors() {
        val center = Coordinate(gridWidth / 2, gridHeight / 2)

        // moore coordinates
        val mooreOriginCords: Set<Coordinate> = setOf(
            Coordinate(center.first - 1, center.second),
            Coordinate(center.first, center.second - 1),
            Coordinate(center.first + 1, center.second),
            Coordinate(center.first, center.second + 1),
        )

        // neumann coordinates
        val neumannOriginCoords: Set<Coordinate> = setOf(
            Coordinate(center.first - 1, center.second),
            Coordinate(center.first, center.second - 1),
            Coordinate(center.first + 1, center.second),
            Coordinate(center.first, center.second + 1),
            Coordinate(center.first - 1, center.second - 1),
            Coordinate(center.first - 1, center.second + 1),
            Coordinate(center.first + 1, center.second - 1),
            Coordinate(center.first + 1, center.second + 1),
        )

        val mooreNeighbors: List<Coordinate> = moore.getNeighbors(grid, center)
        assertEquals(mooreOriginCords, mooreNeighbors.toSet())

        val neumannNeighbors: List<Coordinate> = neumann.getNeighbors(grid, center)
        assertEquals(neumannOriginCoords, neumannNeighbors.toSet())
    }
}